from django.db import models
from django.contrib.auth.models import User
import uuid

# Create your models here.
class Profile(models.Model):
    userID = models.UUIDField(default=uuid.uuid4, unique=True, primary_key=True, editable=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    username = models.CharField(max_length=255, blank=True, null=True)
    balence = models.IntegerField()
    givenName = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.username

class Invoice(models.Model):
    invoiceID = models.UUIDField(default=uuid.uuid4, unique=True, primary_key=True, editable=False)
    sender = models.ForeignKey(Profile, null=False, blank=True, on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(Profile, null=False, blank=True, on_delete=models.CASCADE, related_name='receiver')
    timestamp = models.DateTimeField()
    amount = models.IntegerField()
    note = models.CharField(max_length=255, blank=True, null=True)