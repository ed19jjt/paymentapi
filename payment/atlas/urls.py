from django.urls import path
from . import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [
    path('', views.getRoutes),
    path('users/', views.getUsers), #testing endpoint
    path('create_user/', views.createUser),
    path('authenticate/', views.authenticateUser),
    path('add_funds/', views.addFunds),
    path('make_payment/', views.makePayment),
    path('funds/', views.funds),
    path('check_payment/', views.checkPayment),
]