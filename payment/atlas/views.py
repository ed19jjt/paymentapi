from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.http import JsonResponse
from api.models import Profile, Invoice
from .serializers import ProfileSerializer
from django.contrib.auth import authenticate
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import User
from datetime import datetime

@api_view(['GET'])
def getRoutes(request):
    routes = [ #todo: add all of the correct routes
        {'POST': 'TODO: REMOVE THIS'}
    ]
    return Response(routes)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def getUsers(request):
    users = Profile.objects.all()
    serialize = ProfileSerializer(users, many = True)
    return Response(serialize.data)

@api_view(['POST'])
def createUser(request):
    data = request.data
    username = ''
    password = ''

    if 'username' in data.keys():
        username = data['username']
    elif 'Username' in data.keys():
        username = data['Username']
    else:
        return JsonResponse ({
            'status' : 'No username given'
        })

    if 'password' in data.keys():
        password = data['password']
    elif 'Password' in data.keys():
        password = data['Password']
    else:
        return JsonResponse ({
            'status' : 'No password given'
        })


    newusr = User.objects.create_user(username=username, password=password)
    Profile.objects.create(user=newusr, username=username, balence=0)
    return JsonResponse ({
        'status' : 'Success'
    })

@api_view(['POST'])
def authenticateUser(request):
    data = request.data
    
    username = ''
    password = ''

    if 'username' in data.keys():
        username = data['username']
    elif 'Username' in data.keys():
        username = data['Username']
    else:
        return JsonResponse ({
            'status' : 'No username given'
        })

    if 'password' in data.keys():
        password = data['password']
    elif 'Password' in data.keys():
        password = data['Password']
    else:
        return JsonResponse ({
            'status' : 'No password given'
        })

    user = authenticate(request=request, username=username, password=password)

    if user != None:
        refresh = RefreshToken.for_user(user)
        return JsonResponse ({
            'JWT': str(refresh.access_token),
        })
    
    return JsonResponse ({
        'error': 'Invalid credentials',
    })

@api_view(['POST'])
def addFunds(request):
    user = request.user
    profile = Profile.objects.get(username=user.username)
    profile.balence = profile.balence + int(request.data['Amount'])
    profile.save()

    return JsonResponse ({
        'status' : 'Success'
    })

@api_view(['POST'])
def makePayment(request):
    #take money from the user
    user = request.user
    profile = Profile.objects.get(username=user.username)
    profile.balence -= int(request.data['Amount'])

    #cant spend more than you have
    if profile.balence < 0:
        return JsonResponse({
            'stautus' : 'Insufficient funds'
        })

    profile.save()

    #add money to the destination
    other = Profile.objects.get(username=request.data['DestinationUsername'])
    other.balence += int(request.data['Amount'])
    other.save()

    #make the invoice
    invoice = Invoice.objects.create(sender = profile, receiver=other, timestamp = datetime.now(), amount=request.data['Amount'])
    return JsonResponse({
        'InvoiceID' : invoice.invoiceID
    })

@api_view(['GET'])
def funds(request):
    user = request.user
    profile = Profile.objects.get(username=user.username)

    return JsonResponse({
        'Amount' : profile.balence
    })

@api_view(['GET'])
def checkPayment(request):
    user = request.user
    profile = Profile.objects.get(username=user.username)

    invId = request.GET.get('invoice', '')
    print("-----------", invId)
    invoice = Invoice.objects.get(invoiceID = invId)

    if invoice.sender != profile and invoice.receiver != profile:
        return JsonResponse({
            'status' : 'error'
        })

    return JsonResponse({
        'Sender' : invoice.sender.username,
        'Receiver' : invoice.receiver.username,
        'Timestamp' : invoice.timestamp,
        'Amount' : invoice.amount,
        'Note' : invoice.note
    })