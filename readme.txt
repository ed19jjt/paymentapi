# Details
- The pythonanywhere domain used is: 'https://ed19jjt.pythonanywhere.com/atlas/'.
- The admin account username is: `ammar`, and the password is: `password`.

# Usage
## Usage Notes
- All requests must end with a `/`.
- POST requests must have the content type set to `application/json`.
- JWT tokens must be provided through the header with the key `Authorization`, and the body `Bearer <your-JWT>`
- JWT tokens do expire after a few minutes, if a request is not working you may have to authorize again.
- All funds are presented in pence, as to avoid floating point errors.

## Endpoints
- POST `/create_user/`, which creates a new user, requiring a JSON body of the form:
```json
{
    "Username" : "username",
    "Password" : "password"
}
```

- POST `/authenticate/`, which authenticates as the user, returning a JWT token which should be used in subsequent requests,
requiring a JSON body of the form:
```json
{
    "Username" : "username",
    "Password" : "password"
}
```

- POST `/add_funds/`, which adds money to the account associated with the given JWT token, 
as said in the usage notes the JWT must be passed as a request header,
the endpoint requires a JSON body of the form:
```json
{
    "Amount" : "500"
}
```

- POST `/make_payment/`, which makes a payment from the JWT holder's account to the specified account,
returns an invoice ID which can be used to check that payments have been made, with `/check_payment/`
again the JWT must be passed in the request header,
the endpoint requires a JSON body of the form:
```json
{
    "Amount" : "500",
    "DestinationUsername" : "bob"
}
```

- GET `/funds/`, which returns the amount of money in the JWT holder's account, 
again the JWT must be in the request header.

- GET `/check_payment/?invoice=<your invoice ID>`, which returns the details of the transaction if the JWT
holder was a party in the transaction, again the JWT must be passed in the request header.

## CURL examples
Here are some examples showcasing how a curl command can be constructed to interact with this API.

- Creating a new user:
```
curl --location 'https://ed19jjt.pythonanywhere.com/atlas/create_user/' \
--header 'Content-Type: application/json' \
--data '{
    "Username" : "ammar",
    "Password" : "ammariscool"
}'
```

- Obtaining a JWT:
```
curl --location 'https://ed19jjt.pythonanywhere.com/atlas/authenticate/' \
--header 'Content-Type: application/json' \
--data '{
    "username":"ammar",
    "password":"ammariscool"
}'
```

- Adding funds (and using a JWT):
```
curl --location 'https://ed19jjt.pythonanywhere.com/atlas/add_funds/' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer <your JWT here>' \
--data '{
    "Amount" : "500"
}'
```

- Checking a payment:
```
curl --location 'https://ed19jjt.pythonanywhere.com/atlas/check_payment/?invoice=<your invoice ID here>' \
--header 'Authorization: Bearer <your JWT here>'
```